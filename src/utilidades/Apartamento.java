/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilidades;

import java.sql.ResultSet;
import javax.swing.JOptionPane;

/**
 *
 * @author Ivan
 */
public class Apartamento {
    
    private int codapa;
    private String desapa;
    private boolean estapa;
    private int codtipoa;
    private int codpla;
    private String codsec;
    private int codzon;

    @Override
    public String toString() {
        return "Apartamento{" + "codapa=" + codapa + ", desapa=" + desapa + ", estapa=" + estapa + ", codtipoa=" + codtipoa + ", codpla=" + codpla + ", codsec=" + codsec + ", codzon=" + codzon + '}';
    }
    
    public int getCodapa() {
        return codapa;
    }

    public void setCodapa(int codapa) {
        this.codapa = codapa;
    }

    public String getDesapa() {
        return desapa;
    }

    public void setDesapa(String desapa) {
        this.desapa = desapa;
    }

    public boolean isEstapa() {
        return estapa;
    }

    public void setEstapa(boolean estapa) {
        this.estapa = estapa;
    }

    public int getCodtipoa() {
        return codtipoa;
    }

    public void setCodtipoa(int codtipoa) {
        this.codtipoa = codtipoa;
    }

    public int getCodpla() {
        return codpla;
    }

    public void setCodpla(int codpla) {
        this.codpla = codpla;
    }

    public String getCodsec() {
        return codsec;
    }

    public void setCodsec(String codsec) {
        this.codsec = codsec;
    }

    public int getCodzon() {
        return codzon;
    }

    public void setCodzon(int codzon) {
        this.codzon = codzon;
    }

    public Apartamento(int codapa) {
        this.codapa = codapa;
    }

    public Apartamento(int codapa, String desapa, boolean estapa, int codtipoa, int codpla, String codsec, int codzon) {
        this.codapa = codapa;
        this.desapa = desapa;
        this.estapa = estapa;
        this.codtipoa = codtipoa;
        this.codpla = codpla;
        this.codsec = codsec;
        this.codzon = codzon;
    }

    public Apartamento() {
    }
    
    
    public int insertar_actualizaApartamentos() {
        
        String cmd ="exec ActualizaDepartamento '"+this.codapa+"','"+this.desapa+"','"+this.estapa+"','"+this.codtipoa+"','"+this.codpla+"','"+this.codsec+"','"+this.codzon+"'";

        boolean rs = conexion.ConectarBD.ejecuta_Procedimientos(cmd);
        if (rs == true) {
            JOptionPane.showMessageDialog(null, "Error No Hay datos");
            return 0;
        }
        return 1;
    }
    
    public ResultSet buscar_apartamentos_id(int id,String desc,int est){
        String cmd="select a.codapa,a.desapa,t.destipoa,p.despla,a.codsec,z.deszona,a.estapa from tb_apartamento a inner join tb_tipoApartamento t on a.codtipoa=t.codtipoa inner join tb_planta p on a.codpla=p.codpla inner join tb_zonas z on a.codzon=z.codzona where ("+est+"=1 or estapa=1)";
        
        if(id!=0){
            cmd+=" and codapa="+String.valueOf(id);
        }
        
        if(desc.length()>0){
            cmd+=" and desapa like ('%"+desc+"%')";
        }
        
        return conexion.ConectarBD.ejecuta(cmd);
    }
    
    public ResultSet buscar_apartamentos(int id){
        return conexion.ConectarBD.ejecuta("select * from tb_apartamento where codapa="+id);
    }
}
