/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilidades;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author Ivan
 */
public class Contrato {

    private int codcon;
    private String fincon;
    private String fficon;
    private boolean estcon;
    private String comcon;
    private String feccon;
    private int codcli;
    private int codapa;
    private int codser;
    private float monto;

    public Contrato() {
    }

    public Contrato(int codcon) {
        this.codcon = codcon;
    }

    public Contrato(int codcon, String fincon, String fficon, boolean estcon, String comcon, int codcli, int codapa) {
        this.codcon = codcon;
        this.fincon = fincon;
        this.fficon = fficon;
        this.estcon = estcon;
        this.comcon = comcon;
        this.codcli = codcli;
        this.codapa = codapa;
        this.feccon = "";
    }

    public int getCodcon() {
        return codcon;
    }

    public void setCodcon(int codcon) {
        this.codcon = codcon;
    }

    public String getFincon() {
        return fincon;
    }

    public void setFincon(String fincon) {
        this.fincon = fincon;
    }

    public String getFficon() {
        return fficon;
    }

    public void setFficon(String fficon) {
        this.fficon = fficon;
    }

    public boolean isEstcon() {
        return estcon;
    }

    public void setEstcon(boolean estcon) {
        this.estcon = estcon;
    }

    public String getComcon() {
        return comcon;
    }

    public void setComcon(String comcon) {
        this.comcon = comcon;
    }

    public int getCodcli() {
        return codcli;
    }

    public void setCodcli(int codcli) {
        this.codcli = codcli;
    }

    public int getCodapa() {
        return codapa;
    }

    public void setCodapa(int codapa) {
        this.codapa = codapa;
    }

    @Override
    public String toString() {
        return "Contrato{" + "codcon=" + codcon + ", fincon=" + fincon + ", fficon=" + fficon + ", estcon=" + estcon + ", comcon=" + comcon + ", codcli=" + codcli + ", codapa=" + codapa + '}';
    }

    public int InsertarContrato() {
        // parametros
        String cmd = "exec ContratoActualiza '" + this.codcon + "','" + this.fincon + "','" + this.fficon + "','" + this.estcon + "','" + this.comcon + "','" + this.feccon + "','" + this.codcli + "','" + this.codapa + "'";

        boolean rs = conexion.ConectarBD.ejecuta_Procedimientos(cmd);
        
        if (rs == false) {
            return 0;
        }
        
        return 1;

    }

    public int getCodser() {
        return codser;
    }

    public float getMonto() {
        return monto;
    }

    public void setCodser(int codser) {
        this.codser = codser;
    }

    public void setMonto(float monto) {
        this.monto = monto;
    }
    
    public int InsertarDetalle(){
        // parametros
        String cmd ="exec Detalle_ContratoActualiza '"+this.codcon+"','"+this.codser+"','"+this.monto+"'";
        
        boolean rs = conexion.ConectarBD.ejecuta_Procedimientos(cmd);
        
        if (rs == false) {
            return 0;
        }
        
        return 1;
    }
    
    public ResultSet Buscar_Contrato(String cliente,String fecini,String fechafin){
        return conexion.ConectarBD.ejecuta("select c.codcon,c.fincon,c.fficon,c.codcli,cl.nomcli,a.desapa,isnull(sum(monser),0)as monto from tb_contrato c inner join tb_clientes cl on c.codcli=cl.codcli inner join tb_apartamento a on a.codapa=c.codapa inner join tb_detalle_contrato det on det.codcon=c.codcon where cl.nomcli like ('%"+cliente+"%') and (c.fincon='' or c.fincon>='"+fecini+"') and (c.fficon='' or c.fficon<='"+fechafin+"') group by c.codcon,c.fincon,c.fficon,c.codcli,cl.nomcli,a.desapa");        
    }
    
    public ResultSet Buscar_Contrato_Factura(String fecini){
        return conexion.ConectarBD.ejecuta("select c.codcon,c.fincon,c.fficon,c.codcli,cl.nomcli,a.desapa,isnull(sum(monser),0)as monto from tb_contrato c inner join tb_clientes cl on c.codcli=cl.codcli inner join tb_apartamento a on a.codapa=c.codapa inner join tb_detalle_contrato det on det.codcon=c.codcon where (c.fincon='' or c.fincon>='"+fecini+"') and c.codcon not in(select contrato from tb_factura) group by c.codcon,c.fincon,c.fficon,c.codcli,cl.nomcli,a.desapa");        
    }
}
