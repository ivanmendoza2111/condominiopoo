/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.SQLException;

/**
 *
 * @author Ivan
 */
public class ConectarBD {
    
    public void Conectar(){
        try {
            String connectionUrl = "jdbc:sqlserver://localhost;database=condominioPoo;integratedSecurity=true;";
            Connection conect = DriverManager.getConnection(connectionUrl);
            System.out.println("Conectado.");
        } 
        catch (SQLException ex) {
            System.out.println("Error. "+ex.toString());
        }
    }
    
    public static ResultSet ejecuta(String cmd){
        
           ResultSet rs;
        try {
            String connectionUrl = "jdbc:sqlserver://localhost;database=condominioPoo;integratedSecurity=true;";
            Connection conect = DriverManager.getConnection(connectionUrl);
            Statement comando=conect.createStatement();
            rs=comando.executeQuery(cmd);
            return rs;
        } 
        catch (SQLException ex) {
            return null;
        }
    }
    
    public static boolean ejecuta_Procedimientos(String cmd){
        
           ResultSet rs;
        try {
            String connectionUrl = "jdbc:sqlserver://localhost;database=condominioPoo;integratedSecurity=true;";
            Connection conect = DriverManager.getConnection(connectionUrl);
            Statement comando=conect.createStatement();
            boolean r =comando.execute(cmd);
            return r;
        } 
        catch (SQLException ex) {
            return false;
        }
    }
    
}
