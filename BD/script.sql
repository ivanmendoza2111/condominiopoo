USE [master]
GO
/****** Object:  Database [condominioPoo]    Script Date: 22/4/2017 10:09:53 p. m. ******/
CREATE DATABASE [condominioPoo]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'condominioPoo', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\condominioPoo.mdf' , SIZE = 3264KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'condominioPoo_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\condominioPoo_log.ldf' , SIZE = 1600KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [condominioPoo] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [condominioPoo].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [condominioPoo] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [condominioPoo] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [condominioPoo] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [condominioPoo] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [condominioPoo] SET ARITHABORT OFF 
GO
ALTER DATABASE [condominioPoo] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [condominioPoo] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [condominioPoo] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [condominioPoo] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [condominioPoo] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [condominioPoo] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [condominioPoo] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [condominioPoo] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [condominioPoo] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [condominioPoo] SET  DISABLE_BROKER 
GO
ALTER DATABASE [condominioPoo] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [condominioPoo] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [condominioPoo] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [condominioPoo] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [condominioPoo] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [condominioPoo] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [condominioPoo] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [condominioPoo] SET RECOVERY FULL 
GO
ALTER DATABASE [condominioPoo] SET  MULTI_USER 
GO
ALTER DATABASE [condominioPoo] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [condominioPoo] SET DB_CHAINING OFF 
GO
ALTER DATABASE [condominioPoo] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [condominioPoo] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [condominioPoo] SET DELAYED_DURABILITY = DISABLED 
GO
USE [condominioPoo]
GO
/****** Object:  User [ymadera]    Script Date: 22/4/2017 10:09:53 p. m. ******/
CREATE USER [ymadera] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[tb_apartamento]    Script Date: 22/4/2017 10:09:53 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_apartamento](
	[codapa] [int] NOT NULL DEFAULT ((1)),
	[desapa] [varchar](60) NULL DEFAULT (''),
	[estapa] [bit] NULL DEFAULT ((1)),
	[codtipoa] [int] NULL,
	[codpla] [int] NULL DEFAULT ((1)),
	[codsec] [varchar](1) NULL CONSTRAINT [DF__tb_aparta__codse__6477ECF3]  DEFAULT ('A'),
	[codzon] [int] NULL DEFAULT ((1)),
PRIMARY KEY CLUSTERED 
(
	[codapa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_clientes]    Script Date: 22/4/2017 10:09:53 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_clientes](
	[codcli] [int] NOT NULL,
	[nomcli] [varchar](80) NULL DEFAULT (''),
	[telcli] [varchar](18) NULL DEFAULT (''),
	[corcli] [varchar](60) NULL DEFAULT (''),
	[estcli] [bit] NULL DEFAULT ((1)),
	[dircli] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[codcli] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_contrato]    Script Date: 22/4/2017 10:09:53 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_contrato](
	[codcon] [int] NOT NULL,
	[fincon] [datetime] NULL DEFAULT (getdate()),
	[fficon] [datetime] NULL DEFAULT (getdate()),
	[estcon] [bit] NULL DEFAULT ((1)),
	[comcon] [varchar](500) NULL,
	[feccon] [datetime] NULL DEFAULT (getdate()),
	[codcli] [int] NULL,
	[codapa] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[codcon] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_detalle_contrato]    Script Date: 22/4/2017 10:09:53 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_detalle_contrato](
	[codcon] [int] NOT NULL,
	[codser] [int] NOT NULL,
	[monser] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[codcon] ASC,
	[codser] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_detalle_factura]    Script Date: 22/4/2017 10:09:53 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_detalle_factura](
	[numfac] [int] NOT NULL,
	[codserv] [int] NOT NULL,
	[monto] [float] NULL,
	[descuento] [float] NULL,
	[recargo] [float] NULL,
	[estado] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[numfac] ASC,
	[codserv] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_detalle_recibo]    Script Date: 22/4/2017 10:09:53 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_detalle_recibo](
	[codrec] [int] NOT NULL,
	[numfac] [int] NOT NULL,
	[monto] [float] NULL,
	[estado] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[codrec] ASC,
	[numfac] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tb_empleados]    Script Date: 22/4/2017 10:09:53 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_empleados](
	[codemp] [int] NOT NULL,
	[nomemp] [varchar](80) NULL DEFAULT (''),
	[cedemp] [varchar](13) NULL DEFAULT (''),
	[telemp] [varchar](18) NULL DEFAULT (''),
	[coremp] [varchar](60) NULL DEFAULT (''),
	[estemp] [bit] NULL DEFAULT ((1)),
PRIMARY KEY CLUSTERED 
(
	[codemp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_factura]    Script Date: 22/4/2017 10:09:53 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_factura](
	[numfac] [int] NOT NULL,
	[contrato] [int] NULL,
	[fecha] [datetime] NULL,
	[estado] [int] NULL,
	[detalle] [varchar](500) NULL,
	[monto] [float] NULL,
	[balance] [float] NULL,
	[recargo] [float] NULL,
	[descuento] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[numfac] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_planta]    Script Date: 22/4/2017 10:09:53 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_planta](
	[codpla] [int] NOT NULL DEFAULT ((1)),
	[despla] [varchar](60) NULL DEFAULT (''),
	[estpla] [bit] NULL DEFAULT ((1)),
PRIMARY KEY CLUSTERED 
(
	[codpla] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_recibo]    Script Date: 22/4/2017 10:09:53 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_recibo](
	[numrec] [int] NOT NULL,
	[codcli] [int] NULL,
	[estado] [bit] NULL,
	[monto] [float] NULL,
	[descuento] [float] NULL,
	[recargo] [float] NULL,
	[fecha] [datetime] NULL,
	[detalle] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[numrec] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_secciones]    Script Date: 22/4/2017 10:09:53 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_secciones](
	[codsec] [varchar](2) NULL DEFAULT ('01'),
	[estsec] [bit] NULL DEFAULT ((1))
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_servicios]    Script Date: 22/4/2017 10:09:53 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_servicios](
	[codser] [int] NOT NULL,
	[desser] [varchar](80) NULL DEFAULT (''),
	[estser] [bit] NULL CONSTRAINT [DF__tb_servic__estse__4AB81AF0]  DEFAULT ((1)),
	[monser] [float] NULL DEFAULT ((0)),
	[recser] [float] NULL DEFAULT ((0)),
	[diaser] [int] NULL DEFAULT ((1)),
PRIMARY KEY CLUSTERED 
(
	[codser] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_tipoApartamento]    Script Date: 22/4/2017 10:09:53 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_tipoApartamento](
	[codtipoa] [int] NOT NULL DEFAULT ((1)),
	[destipoa] [varchar](60) NULL DEFAULT (''),
	[esttipoa] [bit] NULL DEFAULT ((1)),
PRIMARY KEY CLUSTERED 
(
	[codtipoa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_usuarios]    Script Date: 22/4/2017 10:09:53 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_usuarios](
	[codusu] [int] NOT NULL,
	[nomusu] [varchar](80) NULL DEFAULT (''),
	[clausu] [varchar](13) NULL DEFAULT (''),
	[estusu] [bit] NULL DEFAULT ((1)),
PRIMARY KEY CLUSTERED 
(
	[codusu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_zonas]    Script Date: 22/4/2017 10:09:53 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_zonas](
	[codzona] [int] NOT NULL DEFAULT ((1)),
	[deszona] [varchar](60) NULL DEFAULT (''),
PRIMARY KEY CLUSTERED 
(
	[codzona] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[tb_apartamento] ([codapa], [desapa], [estapa], [codtipoa], [codpla], [codsec], [codzon]) VALUES (1, N'APTO 1A', 1, 1, 2, N'A', 1)
INSERT [dbo].[tb_apartamento] ([codapa], [desapa], [estapa], [codtipoa], [codpla], [codsec], [codzon]) VALUES (2, N'APTO 1B', 1, 2, 2, N'B', 1)
INSERT [dbo].[tb_apartamento] ([codapa], [desapa], [estapa], [codtipoa], [codpla], [codsec], [codzon]) VALUES (3, N'APTO 2A', 1, 2, 2, N'A', 1)
INSERT [dbo].[tb_apartamento] ([codapa], [desapa], [estapa], [codtipoa], [codpla], [codsec], [codzon]) VALUES (4, N'APTO 2B', 1, 2, 3, N'B', 1)
INSERT [dbo].[tb_apartamento] ([codapa], [desapa], [estapa], [codtipoa], [codpla], [codsec], [codzon]) VALUES (5, N'APTO 3A', 1, 1, 3, N'A', 1)
INSERT [dbo].[tb_clientes] ([codcli], [nomcli], [telcli], [corcli], [estcli], [dircli]) VALUES (1, N'Ivan', N'809-583-5680', N'ivanmendoza2011@hotmail.com', 1, N'Santiago')
INSERT [dbo].[tb_clientes] ([codcli], [nomcli], [telcli], [corcli], [estcli], [dircli]) VALUES (2, N'Yureyny Madera', N'1234', N'yureynymadera@gmail.com', 1, N'Santiago')
INSERT [dbo].[tb_clientes] ([codcli], [nomcli], [telcli], [corcli], [estcli], [dircli]) VALUES (3, N'Susana Mary Tavarez', N'8097654456', N'susana@gmail.com', 1, N'Santiago, Hoya del Caimito')
INSERT [dbo].[tb_clientes] ([codcli], [nomcli], [telcli], [corcli], [estcli], [dircli]) VALUES (4, N'Leonilda', N'809-589-4588', N'leonilda@gmail.com', 1, N'Santiago.')
INSERT [dbo].[tb_clientes] ([codcli], [nomcli], [telcli], [corcli], [estcli], [dircli]) VALUES (5, N'Ingrid Cabrera', N'809-583-8899', N'ingrid@hotmail.com', 1, N'Santiago')
INSERT [dbo].[tb_contrato] ([codcon], [fincon], [fficon], [estcon], [comcon], [feccon], [codcli], [codapa]) VALUES (1, CAST(N'2017-04-22 00:00:00.000' AS DateTime), CAST(N'2017-04-22 00:00:00.000' AS DateTime), 0, N'N', CAST(N'2017-04-22 14:26:13.300' AS DateTime), 1, 1)
INSERT [dbo].[tb_contrato] ([codcon], [fincon], [fficon], [estcon], [comcon], [feccon], [codcli], [codapa]) VALUES (2, CAST(N'2017-04-22 00:00:00.000' AS DateTime), CAST(N'2017-04-22 00:00:00.000' AS DateTime), 0, N'N', CAST(N'2017-04-22 16:19:34.937' AS DateTime), 4, 2)
INSERT [dbo].[tb_contrato] ([codcon], [fincon], [fficon], [estcon], [comcon], [feccon], [codcli], [codapa]) VALUES (3, CAST(N'2017-04-22 00:00:00.000' AS DateTime), CAST(N'2017-04-22 00:00:00.000' AS DateTime), 0, N'N', CAST(N'2017-04-22 17:15:48.410' AS DateTime), 2, 3)
INSERT [dbo].[tb_detalle_contrato] ([codcon], [codser], [monser]) VALUES (1, 1, 8000)
INSERT [dbo].[tb_detalle_contrato] ([codcon], [codser], [monser]) VALUES (2, 2, 1000)
INSERT [dbo].[tb_detalle_contrato] ([codcon], [codser], [monser]) VALUES (3, 1, 8000)
INSERT [dbo].[tb_detalle_contrato] ([codcon], [codser], [monser]) VALUES (3, 5, 250)
INSERT [dbo].[tb_empleados] ([codemp], [nomemp], [cedemp], [telemp], [coremp], [estemp]) VALUES (1, N'Suleyny Madera Vargas', N'03400078123', N'8097368867', N'suly@gmail.com', 1)
INSERT [dbo].[tb_empleados] ([codemp], [nomemp], [cedemp], [telemp], [coremp], [estemp]) VALUES (2, N'Manuel Collado Espinal', N'09844456714', N'8290009999', N'manu@hotmail.com', 1)
INSERT [dbo].[tb_empleados] ([codemp], [nomemp], [cedemp], [telemp], [coremp], [estemp]) VALUES (3, N'Carlos Martinez', N'09999996344', N'8498766666', N'carlos@gmail.com', 1)
INSERT [dbo].[tb_planta] ([codpla], [despla], [estpla]) VALUES (1, N'Primera Planta', 1)
INSERT [dbo].[tb_planta] ([codpla], [despla], [estpla]) VALUES (2, N'Segunda Planta', 1)
INSERT [dbo].[tb_planta] ([codpla], [despla], [estpla]) VALUES (3, N'Tercera Planta', 1)
INSERT [dbo].[tb_secciones] ([codsec], [estsec]) VALUES (N'A', 1)
INSERT [dbo].[tb_secciones] ([codsec], [estsec]) VALUES (N'B', 1)
INSERT [dbo].[tb_servicios] ([codser], [desser], [estser], [monser], [recser], [diaser]) VALUES (1, N'Alquiler', 1, 8000, 30, 30)
INSERT [dbo].[tb_servicios] ([codser], [desser], [estser], [monser], [recser], [diaser]) VALUES (2, N'Mantenimeinto Limpieza', 1, 1000, 200, 30)
INSERT [dbo].[tb_servicios] ([codser], [desser], [estser], [monser], [recser], [diaser]) VALUES (3, N'Mantenimeinto Piscina', 1, 1000, 100, 30)
INSERT [dbo].[tb_servicios] ([codser], [desser], [estser], [monser], [recser], [diaser]) VALUES (4, N'Vigilancia / Seguridad', 1, 1000, 400, 30)
INSERT [dbo].[tb_servicios] ([codser], [desser], [estser], [monser], [recser], [diaser]) VALUES (5, N'Servicio de Piscina', 0, 250, 20, 30)
INSERT [dbo].[tb_servicios] ([codser], [desser], [estser], [monser], [recser], [diaser]) VALUES (6, N'Lavanderia', 1, 750, 25, 30)
INSERT [dbo].[tb_tipoApartamento] ([codtipoa], [destipoa], [esttipoa]) VALUES (1, N'Apartamento Normal', 1)
INSERT [dbo].[tb_tipoApartamento] ([codtipoa], [destipoa], [esttipoa]) VALUES (2, N'PenHouse', 1)
INSERT [dbo].[tb_tipoApartamento] ([codtipoa], [destipoa], [esttipoa]) VALUES (3, N'Compartido', 1)
INSERT [dbo].[tb_usuarios] ([codusu], [nomusu], [clausu], [estusu]) VALUES (1, N'yureynym', N'1234', 1)
INSERT [dbo].[tb_usuarios] ([codusu], [nomusu], [clausu], [estusu]) VALUES (2, N'ivanm', N'1111', 1)
INSERT [dbo].[tb_usuarios] ([codusu], [nomusu], [clausu], [estusu]) VALUES (3, N'wandyr', N'0000', 1)
INSERT [dbo].[tb_zonas] ([codzona], [deszona]) VALUES (1, N'Zona Apartamentos')
INSERT [dbo].[tb_zonas] ([codzona], [deszona]) VALUES (2, N'Zona Parqueo')
INSERT [dbo].[tb_zonas] ([codzona], [deszona]) VALUES (3, N'Zona Recreativa')
INSERT [dbo].[tb_zonas] ([codzona], [deszona]) VALUES (4, N'Zona Verde')
ALTER TABLE [dbo].[tb_recibo] ADD  DEFAULT ((1)) FOR [estado]
GO
ALTER TABLE [dbo].[tb_recibo] ADD  DEFAULT ((0)) FOR [monto]
GO
ALTER TABLE [dbo].[tb_recibo] ADD  DEFAULT ((0)) FOR [descuento]
GO
ALTER TABLE [dbo].[tb_recibo] ADD  DEFAULT ((0)) FOR [recargo]
GO
ALTER TABLE [dbo].[tb_recibo] ADD  DEFAULT (getdate()) FOR [fecha]
GO
ALTER TABLE [dbo].[tb_recibo] ADD  DEFAULT ('') FOR [detalle]
GO
/****** Object:  StoredProcedure [dbo].[ActualizaDepartamento]    Script Date: 22/4/2017 10:09:53 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[ActualizaDepartamento]
@codapa int=0,
@desapa varchar(max)='',
@estapa bit=0,
@codtipoa int=0,
@codpla int=0,
@codsec varchar(max),
@codzon int=0
as

if @codapa= 0 or @codapa is null
begin
	select @codapa=max(codapa) from tb_apartamento
	if @codapa is null set @codapa=0
	set @codapa=@codapa+1
end

if not exists(select codapa from tb_apartamento where codapa=@codapa)
	insert into tb_apartamento(codapa,desapa,estapa,codtipoa,codpla,codsec,codzon)
	values(@codapa,@desapa,@estapa,@codtipoa,@codpla,@codsec,@codzon)
else
	update tb_apartamento set desapa=@desapa,estapa=@estapa,codtipoa=@codtipoa,codpla=@codpla,codsec=@codsec,codzon=@codzon
	where codapa=@codapa

select * from tb_apartamento where codapa=@codapa

GO
/****** Object:  StoredProcedure [dbo].[ActualizaServicios]    Script Date: 22/4/2017 10:09:53 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[ActualizaServicios]
@codser int=0,
@desser varchar(max)='',
@estser bit=0,
@monset float=0,
@recset float=0,
@diaser int=1
as

if @codser = 0 or @codser is null
begin
	select @codser=max(codser) from tb_servicios
	if @codser is null set @codser=0
	set @codser=@codser+1
end

if not exists(select codser from tb_servicios where codser=@codser)
	insert into tb_servicios(codser,desser,estser,monser,recser,diaser)
	values(@codser,@desser,@estser,@monset,@recset,@diaser)
else
	update tb_servicios set desser=@desser,estser=@estser,monser=@monset,recser=@recset,diaser=@diaser
	where codser=@codser

select * from tb_servicios where codser=@codser

GO
/****** Object:  StoredProcedure [dbo].[ClientesActualiza]    Script Date: 22/4/2017 10:09:53 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[ClientesActualiza]
@codcli int=0,
@nomcli varchar(max)='',
@telcli varchar(max)='',
@corcli varchar(max)='',
@estcli bit=1,
@dircli varchar(max)=''
as

if @codcli=0 or @codcli is null
begin
	select @codcli=max(codcli) from tb_clientes
	if @codcli is null set @codcli=0
	set @codcli=@codcli+1
end

if not exists(select codcli from tb_clientes where codcli=@codcli)
	insert into tb_clientes(codcli,nomcli,telcli,corcli,estcli,dircli)
	values(@codcli,@nomcli,@telcli,@corcli,@estcli,@dircli)
else
	update tb_clientes set nomcli=@nomcli,telcli=@telcli,corcli=@corcli,estcli=@estcli,dircli=@dircli
	where codcli=@codcli

select * from tb_clientes where codcli=@codcli


GO
/****** Object:  StoredProcedure [dbo].[ContratoActualiza]    Script Date: 22/4/2017 10:09:53 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[ContratoActualiza]
@codcon	int,
@fincon	datetime,
@fficon	datetime,
@estcon	bit,
@comcon	varchar,
@feccon	datetime,
@codcli	int,
@codapa	int
as

if not exists(select codcon from tb_contrato where codcon=@codcon)
	insert into tb_contrato(codcon,fincon,fficon,estcon,comcon,feccon,codcli,codapa)
					values(@codcon,@fincon,@fincon,@estcon,@comcon,GETDATE(),@codcli,@codapa)


GO
/****** Object:  StoredProcedure [dbo].[Detalle_ContratoActualiza]    Script Date: 22/4/2017 10:09:53 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Detalle_ContratoActualiza]
@codcon	int,
@codser	int,
@monser	float
as

if not exists(select codser from tb_detalle_contrato where codcon=@codcon and codser=@codser)
	insert into tb_detalle_contrato(codcon,codser,monser) values(@codcon,@codser,@monser)

GO
/****** Object:  StoredProcedure [dbo].[Detalle_FacturaActualiza]    Script Date: 22/4/2017 10:09:53 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[Detalle_FacturaActualiza]
@numfac	int,
@codserv	int,
@monto	float,
@descuento	float,
@recargo	float,
@estado	bit
as

if exists(select numfac from tb_factura where numfac=@numfac)
	insert into tb_detalle_factura values(@numfac,@codserv,@monto,0,0,1)

GO
/****** Object:  StoredProcedure [dbo].[FacturaActualiza]    Script Date: 22/4/2017 10:09:53 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[FacturaActualiza]
@numfac	int,
@contrato	int,
@fecha	datetime,
@estado	int,
@detalle	varchar,
@monto	float,
@balance	float
as

if @numfac is null or @numfac=0
begin
	select @numfac=max(numfac) from tb_factura
	if @numfac is null set @numfac=0
	set @numfac=@numfac+1
end


if not exists(select numfac from tb_factura where numfac=@numfac)
	insert into tb_factura values(@numfac,@contrato,GETDATE(),1,@detalle,@monto,@balance,0,0)
else
	update tb_factura set balance=@balance where numfac=@numfac

select * from tb_factura where numfac=@numfac

GO
/****** Object:  StoredProcedure [dbo].[procedimiento_declaracion_j]    Script Date: 22/4/2017 10:09:53 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[procedimiento_declaracion_j]
 @sp varchar(500)='cp_re'
as
set nocount on

set @sp=ltrim(rtrim(@sp))
if @sp='' set @sp='cp_re'
Declare @name varchar(100),@longitud float,@decimales float,@tipo varchar(100),@tipop varchar(100)


Declare cursor_datos cursor for
Select p.name,p.max_length as longitud,p.precision as decimales,t.name as tipo
from sys.parameters p inner join sys.types t on p.system_type_id= t.system_type_id
where p.object_id=object_id(@sp) and t.name !='sysname'
order by p.parameter_id




Declare @declara varchar(4000),@valor varchar(50),@declarecchar varchar(50),@cantidadparametros int,@parametros varchar(4000)
Declare @listavalor varchar(4000)
set @listavalor=''



set @declara=''
open cursor_datos
fetch next from cursor_datos into @name ,@longitud ,@decimales ,@tipo 
set @cantidadparametros=-1
set @tipop=''
set @parametros=''
while @@FETCH_STATUS=0
BEGIN
 set @name=LOWER( LTRIM(rtrim(@name)))
 set @name=UPPER(SUBSTRING(@name,2,1))+LOWER(SUBSTRING(@name,3,LEN(@name)-1))
 
 set @cantidadparametros=@cantidadparametros+1
 if @parametros!=''
  set @parametros=@parametros+','
 
 if @listavalor !='' set @listavalor=@listavalor+','
 set @listavalor=@listavalor+@name
 
 set @parametros=@parametros+nchar(39)+'{'+LTRIM(rtrim(cast(@cantidadparametros as varchar(10))))+'}'+NCHAR(39)
 set @valor='=""'
 set @declarecchar='String '
 if @tipo='boolean' 
 begin
  set @valor='=0'
  set @declarecchar='int '
 end
 if @tipo in ('float') 
 begin
  set @valor='=0'
  set @declarecchar='double '
 end




 if @tipo in ('int') 
 begin
  set @valor='=0'
  set @declarecchar='int '
 end




 if @tipo in ('datetime') 
 begin
  set @valor='=""'
  set @declarecchar='String '
 end
 
 if @name='Numemp' or @name='Pnumemp'
  set @valor='=txtcodemp.Text.Trim()'
 else
 --if @name='Codempuse'
  --set @valor='= FuncionesGenerales.codempuse'
 
 if @name='Idioma'
  set @valor='= FuncionesGenerales.objUsuarioActual.Idiomauser'
 
 
 if LEN(ltrim(rtrim(@declara)))<7
  set @declara=left(@declara+space(10),7)




 /*if LEN(ltrim(rtrim(@name)))<20
  set @name=left(@name+space(30),20)*/
 set @name=NCHAR(9)+@name
 if @tipop='' set @tipop=@tipo
 if @declara !='' set @declara=@declara+','
 
 
 set @declara=@declara+REPLACE(@name,'@','')+NCHAR(9)+NCHAR(9)+@valor
 print @declarecchar+' '+@name+NCHAR(9)+NCHAR(9)+@valor+';'
 
   fetch next from cursor_datos into @name ,@longitud ,@decimales ,@tipo 
   if @@FETCH_STATUS!=0 or @tipo!=@tipop
   begin
    set @declara= @declarecchar+@declara+';'
    --print @declara
    
    set @declara=''
    
   end
   set @tipop=@tipo
end
CLOSE cursor_datos
DEALLOCATE cursor_datos
print '// parametros'

declare @cmd varchar(4000)
set @cmd='String cmd ='+nchar(34)+'exec '+@sp+' '+@parametros+NCHAR(34)+','+NCHAR(10)+NCHAR(13)
set @cmd=@cmd+@listavalor+';'
--string cmd = string.Format("exec sb_cuentas '{0}','{1}'", DataAccessBase.numemp, DataAccessBase.codempuse);
print @cmd
print ' try {'
print 'ResultSet rs = conexion.ConectarBD.ejecuta(cmd);'
print 'if (rs.next()==false){'
print '  JOptionPane.showMessageDialog(null, "Error No Hay datos");'
print '  return;'
print ' }'
print '}'
print 'catch (SQLException ex){'
print '	Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);'
print '}'
GO
/****** Object:  StoredProcedure [dbo].[SelUsuarios]    Script Date: 22/4/2017 10:09:53 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SelUsuarios]
@nomusu varchar(200)='',
@cont varchar(200)=''
as
begin
select * 
from tb_usuarios
where nomusu=@nomusu and clausu=@cont and estusu=1
end

GO
USE [master]
GO
ALTER DATABASE [condominioPoo] SET  READ_WRITE 
GO
